#ifndef __BACKGROUND_REMOVAL_CONFIG_H__
#define __BACKGROUND_REMOVAL_CONFIG_H__

/* Version */
#define VERSION "0.0.1~2.g8082ffd"

/* Package name */
#define PACKAGE "background-removal"

/* The gettext domain name */
#define GETTEXT_PACKAGE "background-removal"

/* Library installation directory */
#define KURENTO_MODULES_SO_DIR "/usr/local/lib/kurento/modules"

#endif /* __BACKGROUND_REMOVAL_CONFIG_H__ */
