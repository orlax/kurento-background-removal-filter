# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ubuntu/kurento-filter-background-removal/background-removal

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ubuntu/kurento-filter-background-removal/background-removal

# Include any dependencies generated for this target.
include src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/depend.make

# Include the progress variables for this target.
include src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/progress.make

# Include the compile flags for this target's objects.
include src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/flags.make

src/server/cpp_server_internal.generated: src/server/interface/backgroundremoval.kmd.json
src/server/cpp_server_internal.generated: src/server/interface/backgroundremoval.backgroundRemoval.kmd.json
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/ubuntu/kurento-filter-background-removal/background-removal/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Generating cpp_server_internal.generated, implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp, implementation/generated-cpp/backgroundRemovalImplInternal.cpp, implementation/generated-cpp/backgroundRemovalImplFactory.hpp"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/cmake -E touch cpp_server_internal.generated
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/kurento-module-creator -c /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp -r /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/interface -dr /usr/share/kurento/modules -dr /usr/share/kurento/modules -it cpp_server_internal

src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp: src/server/cpp_server_internal.generated
	@$(CMAKE_COMMAND) -E touch_nocreate src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp

src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp: src/server/cpp_server_internal.generated
	@$(CMAKE_COMMAND) -E touch_nocreate src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp

src/server/implementation/generated-cpp/backgroundRemovalImplFactory.hpp: src/server/cpp_server_internal.generated
	@$(CMAKE_COMMAND) -E touch_nocreate src/server/implementation/generated-cpp/backgroundRemovalImplFactory.hpp

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/flags.make
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o: src/server/implementation/objects/backgroundRemovalOpenCVImpl.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/ubuntu/kurento-filter-background-removal/background-removal/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o -c /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/objects/backgroundRemovalOpenCVImpl.cpp

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.i"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/objects/backgroundRemovalOpenCVImpl.cpp > CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.i

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.s"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/objects/backgroundRemovalOpenCVImpl.cpp -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.s

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.requires:

.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.requires

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.provides: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.requires
	$(MAKE) -f src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build.make src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.provides.build
.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.provides

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.provides.build: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o


src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/flags.make
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o: src/server/implementation/objects/backgroundRemovalImpl.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/ubuntu/kurento-filter-background-removal/background-removal/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CXX object src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o -c /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/objects/backgroundRemovalImpl.cpp

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.i"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/objects/backgroundRemovalImpl.cpp > CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.i

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.s"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/objects/backgroundRemovalImpl.cpp -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.s

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.requires:

.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.requires

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.provides: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.requires
	$(MAKE) -f src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build.make src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.provides.build
.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.provides

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.provides.build: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o


src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/flags.make
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o: src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/ubuntu/kurento-filter-background-removal/background-removal/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building CXX object src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o -c /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.i"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp > CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.i

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.s"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.s

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.requires:

.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.requires

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.provides: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.requires
	$(MAKE) -f src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build.make src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.provides.build
.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.provides

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.provides.build: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o


src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/flags.make
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o: src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/ubuntu/kurento-filter-background-removal/background-removal/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building CXX object src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o -c /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.i"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp > CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.i

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.s"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp -o CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.s

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.requires:

.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.requires

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.provides: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.requires
	$(MAKE) -f src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build.make src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.provides.build
.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.provides

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.provides.build: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o


# Object files for target kmsbackgroundremovalimpl
kmsbackgroundremovalimpl_OBJECTS = \
"CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o" \
"CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o" \
"CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o" \
"CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o"

# External object files for target kmsbackgroundremovalimpl
kmsbackgroundremovalimpl_EXTERNAL_OBJECTS =

src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build.make
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmselementsimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmsfiltersimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmselementsimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/libkmsbackgroundremovalinterface.a
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmselementsimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmsfiltersimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmselementsimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: /usr/lib/x86_64-linux-gnu/libkmsfiltersimpl.so
src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/ubuntu/kurento-filter-background-removal/background-removal/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Linking CXX shared library libkmsbackgroundremovalimpl.so"
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/kmsbackgroundremovalimpl.dir/link.txt --verbose=$(VERBOSE)
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && $(CMAKE_COMMAND) -E cmake_symlink_library "libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd" libkmsbackgroundremovalimpl.so.0 libkmsbackgroundremovalimpl.so

src/server/libkmsbackgroundremovalimpl.so.0: src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd
	@$(CMAKE_COMMAND) -E touch_nocreate src/server/libkmsbackgroundremovalimpl.so.0

src/server/libkmsbackgroundremovalimpl.so: src/server/libkmsbackgroundremovalimpl.so.0.0.1~2.g8082ffd
	@$(CMAKE_COMMAND) -E touch_nocreate src/server/libkmsbackgroundremovalimpl.so

# Rule to build all files generated by this target.
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build: src/server/libkmsbackgroundremovalimpl.so

.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/build

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/requires: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalOpenCVImpl.cpp.o.requires
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/requires: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/objects/backgroundRemovalImpl.cpp.o.requires
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/requires: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp.o.requires
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/requires: src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/implementation/generated-cpp/backgroundRemovalImplInternal.cpp.o.requires

.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/requires

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/clean:
	cd /home/ubuntu/kurento-filter-background-removal/background-removal/src/server && $(CMAKE_COMMAND) -P CMakeFiles/kmsbackgroundremovalimpl.dir/cmake_clean.cmake
.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/clean

src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/depend: src/server/cpp_server_internal.generated
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/depend: src/server/implementation/generated-cpp/SerializerExpanderBackgroundremoval.cpp
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/depend: src/server/implementation/generated-cpp/backgroundRemovalImplInternal.cpp
src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/depend: src/server/implementation/generated-cpp/backgroundRemovalImplFactory.hpp
	cd /home/ubuntu/kurento-filter-background-removal/background-removal && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ubuntu/kurento-filter-background-removal/background-removal /home/ubuntu/kurento-filter-background-removal/background-removal/src/server /home/ubuntu/kurento-filter-background-removal/background-removal /home/ubuntu/kurento-filter-background-removal/background-removal/src/server /home/ubuntu/kurento-filter-background-removal/background-removal/src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/server/CMakeFiles/kmsbackgroundremovalimpl.dir/depend

